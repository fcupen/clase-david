import { Component, OnInit } from '@angular/core';
import { ResponsiveModule } from 'ng2-responsive';

import { Observable } from 'rxjs/Observable';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase,AngularFireList ,AngularFireObject } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { DatabaseService } from '../services/database.service';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.css']
})
export class SetupComponent implements OnInit {

  itemsRef: AngularFireList<any>;
  
  constructor(public DatabaseService:DatabaseService, private database: AngularFireDatabase) { 
  

    this.itemsRef = this.database.list('/');
    //this.items = this.itemsRef.valueChanges(); 
    this.itemsRef.valueChanges().subscribe(profile =>{ 
    });
  }

  
  user:string; 
  ngOnInit() {
    this.DatabaseService.cast.subscribe(user=> this.user = user);
  }
  
}
