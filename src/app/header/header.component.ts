import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../services/database.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public DatabaseService:DatabaseService) { }

  user:string; 

  ngOnInit() {
    this.DatabaseService.cast.subscribe(user=> this.user = user);
  } 

}
