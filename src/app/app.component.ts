import { Component, ViewChild,ElementRef } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase,AngularFireList ,AngularFireObject } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
 
import { ResponsiveModule } from 'ng2-responsive'; 
import { DatabaseService } from './services/database.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[DatabaseService]
})
export class AppComponent {
  @ViewChild('midiv') div:ElementRef;
  color:string="blue";
  itemsRef: AngularFireList<any>;
  divElement
 profileObject: AngularFireObject<any>;
 item:any;
  constructor(public DatabaseService:DatabaseService, private database: AngularFireDatabase) {
    this.padding();

    this.divElement=this.div;
    console.log(this.divElement)

    this.itemsRef = this.database.list('test/');
    //this.items = this.itemsRef.valueChanges(); 
    this.itemsRef.valueChanges().subscribe(profile =>{
      console.log(profile);
      this.item=profile;
      //let items = profile[1];
      
    });
     /*
    database.list<any>('/').snapshotChanges().subscribe(profile =>{
      console.log(profile);
      //console.log(this.items);
    })*/
    
    //this.saveGame('test',{'test2':{'test2':'test3','test23':'test233'},'test23':{'test2':'test3','test23':'test233'}});
  }
  title = 'app';
  async saveGame(id, info){
    this.profileObject = this.database.object('/'+id);
    try{
      await this.profileObject.set(info);
      return true;
    }
    catch(e){
      console.error(e);
      return false;
    }
  }
  
padd="0px";

aux=true;
public padding(){
  setInterval(()=>{

if(this.aux){
  this.aux=!this.aux;
this.padd= "100px";
}else{
  this.aux=!this.aux;
  this.padd= "0px";
}

  },1000);
}
padd2="red"
miclick(midiv){
  console.log(midiv);
this.padd2="green";
}
}
