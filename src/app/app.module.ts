import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule }    from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { AngularFireModule} from 'angularfire2';  
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuth } from 'angularfire2/auth';
import {AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { ContentComponent } from './content/content.component';

import { routing } from './app.routes';

import { FIREBASE_CONFIG } from './firebase.config';

import { ResponsiveModule } from 'ng2-responsive';
import { SetupComponent } from './setup/setup.component';
import { InicioComponent } from './inicio/inicio.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    SetupComponent,
    InicioComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    ResponsiveModule, 
    FormsModule,
     HttpModule,
    JsonpModule,
    routing,
    AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
