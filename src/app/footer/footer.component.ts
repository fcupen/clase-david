import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../services/database.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(public DatabaseService:DatabaseService) { }

  user:string; 

  ngOnInit() {
    this.DatabaseService.cast.subscribe(user=> this.user = user);
  } 

}
