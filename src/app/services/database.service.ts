import { Injectable } from '@angular/core';

import { AngularFireDatabase,AngularFireObject } from 'angularfire2/database';
import { AngularFireDatabaseModule,  AngularFireList  } from 'angularfire2/database';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class DatabaseService {

  constructor(private database: AngularFireDatabase) { }

profileObject: AngularFireObject<any>
async saveProfile(user, profile){ 
  this.profileObject = this.database.object(`/info_cliente/${user}`);
  try{
    await this.profileObject.set(profile);
    return true;
  }
  catch(e){
    console.error(e);
    return false;
  }
}

  setcolor(colores){ 
    localStorage.setItem('colores',JSON.stringify(colores));
    //localStorage.removeItem("profile");
  }
  getcolor(){
    return JSON.parse(localStorage.getItem("colores")) ;
  }





  private user = new BehaviorSubject<string>('cliente');
  cast = this.user.asObservable();
 

  editUser(newUser){
    this.user.next(newUser);
  }
}
