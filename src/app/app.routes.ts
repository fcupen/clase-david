
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { SetupComponent } from './setup/setup.component';

// Route Configuration
export const routes: Routes = [
    {
        path: '',
        component: InicioComponent
    },
    { path: 'inicio', component: InicioComponent },
    { path: 'setup', component: SetupComponent }
];

// Deprecated provide
// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(routes)
// ];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);