import { Component, OnInit, Input, AfterViewInit, OnDestroy, DoCheck } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase,AngularFireList ,AngularFireObject } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
 
import { ResponsiveModule } from 'ng2-responsive'; 
import { DatabaseService } from '../services/database.service'; 


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit , AfterViewInit, OnDestroy, DoCheck{
 
  itemsRef: AngularFireList<any>;
 
 profileObject: AngularFireObject<any>;
 
  constructor(public DatabaseService:DatabaseService, private database: AngularFireDatabase, private router: Router) {
     
    this.itemsRef = this.database.list('/');
    //this.items = this.itemsRef.valueChanges(); 
    this.itemsRef.valueChanges().subscribe(profile =>{ 
      
      let items = profile[1]; 
    });
     
    database.list<any>('/').valueChanges().subscribe(profile =>{
      console.log(profile);
      
    })

    let aux=0;
    this.miInterval=setInterval(()=>{
      aux++
      console.log("hola numero: "+ aux);
    },1000);
     }

     btn(){
      this.editTheUser('admin');
    this.router.navigate(['setup/']);
     }
     miInterval;
  title = 'app';
  async saveGame(id, info){
    this.profileObject = this.database.object(`/`);
    try{
      await this.profileObject.set(info);
      return true;
    }
    catch(e){
      console.error(e);
      return false;
    }
  }

  user:string; 
  ngOnInit() {
    this.DatabaseService.cast.subscribe(user=> this.user = user);
  } 
  ngAfterViewInit(){
    console.log("llamando AfterViewInit");
  }
  ngOnDestroy(){
    clearInterval(this.miInterval);
    console.log("llamando OnDestroy");
  }
  ngDoCheck(){
    console.log("llamando ngDoCheck");
  }

  editTheUser(editUser){
    this.DatabaseService.editUser(editUser);
  }
}
